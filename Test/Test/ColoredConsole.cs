﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace Test
{
    public static class ColoredConsole
    {
        private static readonly ConcurrentQueue<Tuple<string, ConsoleColor>> _mainQueue = new ConcurrentQueue<Tuple<string, ConsoleColor>>();
        private static object _lock = new object();
        private static AutoResetEvent evnt = new AutoResetEvent(true);

        private static Worker work = new Worker();

        private static Thread t = new Thread(work.Do);

        private static bool stop = false;

        static ColoredConsole()
        {
            t.Start();
        }

        public static void Stop()
        {
            stop = true;
            evnt.Set();
        }

        private static void Enque(string text, ConsoleColor color)
        {
            lock (_lock)
            {
                _mainQueue.Enqueue(new Tuple<string, ConsoleColor>(text, color));
                evnt.Set();
            }
        }

        public static void WriteLine(object val1, ConsoleColor color)
        {
            Enque(val1.ToString(), color);
        }

        public static string ReadLine()
        {
            return System.Console.ReadLine();
        }

        public static void WriteLine(object val1)
        {
            Enque(val1.ToString(), ConsoleColor.Gray);
        }

        public static ConsoleKeyInfo ReadKey()
        {
            return System.Console.ReadKey();
        }

        private class Worker
        {
            public void Do()
            {
                while (true)
                {
                    evnt.WaitOne();
                    if (stop)
                    {
                        return;
                    }
                    lock (_lock)
                    {
                        Tuple<string, ConsoleColor> tpl = null;
                        if (_mainQueue.TryDequeue(out tpl))
                        {
                            if (tpl != null)
                            {
                                System.Console.ForegroundColor = tpl.Item2;
                                System.Console.WriteLine(tpl.Item1);
                            }
                            if (_mainQueue.Count > 0)
                            {
                                evnt.Set();
                            }
                            else
                            {
                                evnt.Reset();
                            }
                        }
                    }
                }
            }
        }
    }

}
