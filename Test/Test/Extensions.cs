﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    public static class Extensions
    {
        public static TV GetValue<TK, TV>(this IDictionary<TK, TV> dict, TK key, TV defaultValue = default(TV))
        {
            TV value;
            return dict.TryGetValue(key, out value) ? value : defaultValue;
        }

        public static bool IsInteger(string value)
        {
            int result;
            return int.TryParse(value, out result);
        }
    }
}
