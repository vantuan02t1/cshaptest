﻿using System;
using System.Collections.Generic;
using Console = Test.ColoredConsole;

namespace Test
{
    class Program
    {
        // Define task number
        private static readonly Dictionary<string, Action> TasksMap = new Dictionary<string, Action>
        {
            ["1"] = () => RevertString(),
            ["2"] = () => CheckPrimeNumber(),
            ["3"] = () => WriteQuickSortResult(),
        };

        /// <summary>
        /// Main function
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {
                RunTask(args);
            }
            catch (ApplicationException e)
            {
                Console.WriteLine((object)e.Message, ConsoleColor.Red);
            }
            catch (Exception e)
            {
                Console.WriteLine((object)e.RecursiveErrorToString(), ConsoleColor.Red);
            }
            Console.ReadLine();
            Console.Stop();
        }

        /// <summary>
        /// Run Task
        /// </summary>
        /// <param name="args"></param>
        private static void RunTask(string[] args)
        {
            Console.WriteLine("Please write the task number you want to run (1: Revert String; 2: Is Prime; 3: Quick Sort with fixed array number):");
            var taskNumber = Console.ReadLine();
            var taskAction = TasksMap.GetValue(taskNumber);
            if (taskAction == null)
            {
                throw new ApplicationException("Associated task was not found");
            }

            Console.WriteLine($"Press any key to start task for '{taskNumber}'");
            Console.ReadKey();
            Console.WriteLine(string.Empty);

            Console.WriteLine($"Task '{taskNumber}' started");
            taskAction();
            Console.WriteLine($"Task '{taskNumber}' completed");
        }

        /// <summary>
        /// Revert String
        /// </summary>
        private static void RevertString()
        {
            Console.WriteLine("Please write the string you want to revert:");
            var str = Console.ReadLine();
            if (!string.IsNullOrEmpty(str))
            {
                // Revert string
                char[] charArray = str.ToCharArray();
                for (int i = 0, j = str.Length - 1; i < j; i++, j--)
                {
                    charArray[i] = str[j];
                    charArray[j] = str[i];
                }
                string reversedstring = new string(charArray);
                Console.WriteLine(reversedstring);
            }
            else
            {
                Console.WriteLine("String is null or empty");
            }
        }

        /// <summary>
        /// Check prime number
        /// </summary>
        private static void CheckPrimeNumber()
        {
            Console.WriteLine("Please write the integer number you want to check is prime:");
            var numberStr = Console.ReadLine();
            if (!string.IsNullOrEmpty(numberStr) && Extensions.IsInteger(numberStr))
            {
                if (Utils.IsPrime(Convert.ToInt32(numberStr)))
                {
                    Console.WriteLine($"The number '{numberStr}' is a prime");
                }
                else
                {
                    Console.WriteLine($"The number '{numberStr}' is not a prime");
                }
            }
            else
            {
                Console.WriteLine("Incorrect integer number!");
            }
        }

        /// <summary>
        /// Quick Sort
        /// </summary>
        private static void WriteQuickSortResult()
        {
            int[] arr = { 90, 21, 15, 77, 22, 0, 66, 100, 50, 9 };
            Console.WriteLine("You will got result from fixed array number here: 90, 21, 15, 77, 22, 0, 66, 100, 50, 9");
            int n = 10, i;
            Console.WriteLine("Quick Sort");
            Console.WriteLine("Initial array is: ");
            for (i = 0; i < n; i++)
            {
                Console.WriteLine(arr[i] + " ");
            }
            Utils.QuickSort(arr, 0, 9);
            Console.WriteLine("Sorted Array is: ");
            for (i = 0; i < n; i++)
            {
                Console.WriteLine(arr[i] + " ");
            }
        }
    }
}
