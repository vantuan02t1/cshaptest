﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    public static class ExceptionExtentions
    {
        public static string RecursiveErrorToString(this Exception ex)
        {
            if (ex == null) return string.Empty;
            var sb = new StringBuilder();
            sb.AppendLine(ex.GetType().FullName);
            sb.Append(":");
            sb.Append(ex.Message);
            if (!string.IsNullOrEmpty(ex.HelpLink))
            {
                sb.AppendLine("Help link:");
                sb.Append(ex.HelpLink);
            }
            if (ex.InnerException != null)
            {
                sb.AppendLine("-->\n");
                sb.Append(ex.InnerException.RecursiveErrorToString());
                if (string.IsNullOrEmpty(ex.InnerException.StackTrace))
                {
                    sb.AppendLine("StackTrace:");
                    sb.Append(ex.StackTrace);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ex.StackTrace) && ex.InnerException == null)
                {
                    sb.AppendLine("StackTrace:");
                    sb.Append(ex.StackTrace);
                }
            }
            return sb.ToString();
        }

    }
}
